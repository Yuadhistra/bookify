import React, { useState } from 'react';
import 'react-toastify/dist/ReactToastify.css';
import { NavLink } from 'react-router-dom';
import Cookies from 'js-cookie';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';
import { ToastContainer, toast } from 'react-toastify';

function LoginForm({setIsLoggedIn, setShowLoginForm}) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [emailError, setEmailError] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const[errorMessage,setErrorMessage]=useState('')
  const [isLoading, setIsLoading] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [incorrectPasswordError, setIncorrectPasswordError] = useState('');

  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };

  const validateEmail = () => {
    if (!email) {
      setEmailError('Email is required');
    } else if (!/\S+@\S+\.\S+/.test(email)) {
      setEmailError('Invalid email address');
    } else {
      setEmailError('');
    }
  };
  
  const validatePassword = () => {
    if (!password) {
      setPasswordError('Password is required');
    } else {
      setPasswordError('');
    }
  };

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };
  
  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };
  
  const navigate = useNavigate();
  const handleSubmit = async (event) => {
    event.preventDefault();
    validateEmail();
    validatePassword();
    
    if (!emailError && !passwordError) {
      try {
        setIsLoading(true);
        const response = await axios.post('http://localhost:5000/login', {
          email,
          password,
        });
        console.log(response.data); 

        const authToken = response.data.authToken;
        localStorage.setItem('useremail', response.data.email);
        Cookies.set('authToken', authToken);
        localStorage.setItem('authToken', authToken);
        console.log(authToken);
        setIsLoading(true);
   
        toast.success('Login Successfully.')
        setTimeout(()=>{
            navigate('/home2');
        },2000);
      
      } catch (error) {
        console.error(error);
        if (error.response && error.response.status === 401) {
          setIncorrectPasswordError('Incorrect email or password');
        } else {
          setErrorMessage('Error occurred while logging in');
        }
      }finally{
        setIsLoading(false)
      }
    }
  };
  
const Image = styled.img`
      width:30%;
      position:relative;
      left:10rem;
`;

const Button = styled.button`
  outline: 0;
  background:#601A49;
  width: 100%;
  border:0;
  border-radius: 3px;
  padding: 15px;
  color: #ffffff;
  font-size: 15px;
  transition: all 0.4s ease-in-out;
  cursor: pointer;

  &:hover,
    &:active,
    &:focus {
      background: #ce1992;
      color: #ffffff;
      border: 1px solid #ce1992;
  }
`;

  return (
    <div style={{display:'flex',alignItems:'center',justifyContent:'center',
 
    height:'100vh',width:'100%'}}>
      <ToastContainer position='top-center'/>
      <form style={{width:'500px',height:'550px', background:'white', boxShadow:'rgba(0, 0, 0, 0.19) 0px 10px 20px, rgba(0, 0, 0, 0.23) 0px 6px 6px', padding:'40px'}} onSubmit={handleSubmit}>
      <h1 style={{display:'flex', justifyContent:'center', alignItems:'center', color: '#601A49'}}>Login</h1>
      <div className='cont'>
        <br/>
        <input style={{width:'100%',padding:'10px',borderRadius:'4px', margin:'15px 0', fontSize:'18px', fontFamily:"Roboto, sans-serif"}} type="text" id="email" value={email} onChange={(e) => setEmail(e.target.value)} onBlur={validateEmail} placeholder='Email' className='input'/>
        <div>
            {emailError && <span style={{color:'red'}}>{emailError}</span>}
            <br/>
            
            <div class="row">
                <div class="col-11" >
                    <input style={{width:'110%',padding:'10px', fontSize:'18px', borderRadius:'4px', margin:'15px 0', fontFamily:"Roboto, sans-serif"}} type={showPassword ? 'text' : 'password'} id="password" value={password} onChange={(e) => setPassword(e.target.value)} onBlur={validatePassword} placeholder='Password' className='input' />
                </div>
                <div class="col-1" style={{display:'flex',alignItems:'center'}}>
                    <FontAwesomeIcon style={{position:'relative',right:'1rem'}} icon={showPassword ? faEyeSlash : faEye} onClick={togglePasswordVisibility} />
                </div>
            </div>
        </div>

        <span style={{color:'red'}} className="error">{passwordError}</span>
        {incorrectPasswordError && <span style={{ color: 'red' }}>{incorrectPasswordError}</span>}
        <Button style={{marginTop:'2rem',marginBottom:'2rem',padding:'15px',width:'100%'}} type="submit">{isLoading ?'Loading...':'Log In'}</Button>
        <p style={{color:'#989898',fontSize:'16px',position:'relative',bottom:'2rem', fontFamily:"Roboto, sans-serif",fontSize:'18px',marginTop:'20px'}}>If you don't have an account &nbsp;
        <NavLink to="/register">
          Register
        </NavLink>
        </p>
      </div> 
      </form>
    </div>
  );
}

export default LoginForm;
