import React from "react";
import { Routes, Route } from "react-router-dom";
import Home2 from "./pages/Home";
import About from "./pages/About";
import Contact from "./pages/Contact";
import LoginForm from "./sign/Login";
import RegistrationForm from "./sign/Registration";
import Notfound from "./pages/404";
import Order from "./pages/order";

const App = () => {
  return (
      <div className="app">
          <Routes>
              <Route path="/">
              <Route index element={<LoginForm />} />
              <Route path="/login" element={<LoginForm />}/>
              <Route path="/register" element={<RegistrationForm />}/>
              <Route path="/home2" element={<Home2/>}/>
              <Route path="/about" element={<About/>}/>
              <Route path="/contact" element={<Contact/>}/>
              <Route path="/order" element={<Order/>}/>
              <Route path="*" element={<Notfound />} />
              </Route>
          </Routes>     
      </div>
  );
};

export default App;