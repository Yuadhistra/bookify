import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Navbar, Nav,Dropdown } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import bb from '../../photo/n2.png';
import pp from '../../photo/u.png';
import styled from 'styled-components';

const NavbarContainer = styled.div`
  background-color: #601A49;
`;

export default function Header() {
  return (
    <NavbarContainer className='container-fluid px-5 shadow' >
      <Navbar expand="lg">
        <Navbar.Brand href="#"><img style={{ height: '6rem', width: '6rem' }} src={bb} alt="logo" /></Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto d-flex gap-md-5">
            <NavLink exact to="/home2" activeClassName="active-link text-white fw-bold" className="nav-link text-white">Home</NavLink>
            <NavLink exact to="/order" activeClassName="active-link text-white fw-bold" className="nav-link text-white">My Order</NavLink>
            <NavLink exact to="/about" activeClassName="active-link text-white fw-bold" className="nav-link text-white">About</NavLink>
            <NavLink exact to="/contact" activeClassName="active-link text-white fw-bold" className="nav-link text-white">Contact</NavLink>
          </Nav>
          <Dropdown align="end">
            <Dropdown.Toggle variant="link" id="profile-dropdown">
              <img style={{ height: '2rem', width: '2rem' }} src={pp} alt="logo" />
            </Dropdown.Toggle>
            <Dropdown.Menu>
              <Dropdown.Item href="#">Profile</Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item href="/login">Logout</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </Navbar.Collapse>
      </Navbar>
    </NavbarContainer>
  );
}