import React from 'react';
import hero from '../../photo/1.jpg';
import hero2 from '../../photo/2.jpg';
import hero3 from "../../photo/3.jpg"
const HeroSlider = () => {
  return (
    <div id="carouselExampleCaptions" className="carousel slide mt-2">
      <div className="carousel-indicators">
        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
      </div>
      <div className="carousel-inner" >
        <div className="carousel-item active">
          <img src={hero} className="d-block w-100" alt="..." />
          <div className="carousel-caption d-none d-md-block">
            <h4>Best Way To Get Your Desired Book.</h4>
            <p>"Welcome to our online bookstore, where stories come to life. Explore a vast collection of books spanning various genres, from captivating novels to insightful non-fiction. Immerse yourself in the world of words, where imagination knows no bounds."</p>
          </div>
        </div>
        <div className="carousel-item">
          <img src={hero2} className="d-block w-100" alt="..." />
          <div className="carousel-caption d-none d-md-block">
            <h5>Discover magical Books</h5>
            <p>"Discover hidden treasures, embark on literary adventures, and expand your horizons with every page. Whether you are seeking an enthralling mystery, a heartwarming romance, or an enlightening guide, our bookstore is your gateway to literary wonders."</p>
          </div>
        </div>
        <div className="carousel-item">
          <img src={hero3} className="d-block w-100" alt="..." />
          <div className="carousel-caption d-none d-md-block">
            <h5>Discover your knowledge</h5>
            <p>"Discover a world of knowledge at your fingertips. Explore our vast collection of books and immerse yourself in captivating stories, insightful knowledge, and inspiring ideas. Start your reading journey today and let the magic of books ignite your imagination"</p>
          </div>
        </div>
      </div>
      <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
        <span className="visually-hidden">Previous</span>
      </button>
      <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
        <span className="carousel-control-next-icon" aria-hidden="true"></span>
        <span className="visually-hidden">Next</span>
      </button>
    </div>
  );
};

export default HeroSlider;