import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebook, faTwitter, faInstagram } from '@fortawesome/free-brands-svg-icons';
import './Footer.css'
import logo from '../../photo/n2.png';

export default function Footer() {

  return (
   
    <div style={{backgroundColor: '#601A49'}}>
      <div className="row g-4 px-5 my-4" >
        <div className="col-md-3">
          <div className="card-body" style={{ color: 'black' }}>
              <img src={logo} className="mx-0" style={{ height: '6rem', width: '6rem' }} alt=""/>
          </div>


        </div>
      <div className="col-md-3">
    
        <div className="link-body" style={{color:'white'}}>
          <h5 className="link-title">Quick Links</h5>
          <a href="https://markhampubliclibrary.ca/blogs/post/top-10-benefits-of-reading-for-all-ages/" className="link-text">Benefits of Book reading</a><br/>
          <a href="https://bookshop.org/pages/bookstores" className="link-text">Bookshop.org</a><br/>
        </div>
      </div>
      <div className="col-md-3" >
        
        <div className="link-body" style={{color:'white'}}>
          <h5 className="link-title">Service</h5>
          <a href="https://www.cerinastudio.com/?gclid=EAIaIQobChMI3__rn73D_wIVDh4rCh1k3gUREAAYAiAAEgLoifD_Bw" className="link-text">Chatbot</a><br/>
          <a href="#" className="link-text">Go somewhere</a><br/>
        </div>
        
      </div>
      <div className="col-md-3">
        
        <div className="card-body">
          <h5 className="card-title" style={{color:'white'}}>Connect to us</h5>
        
          <div className='social-icons'>
            <a href="https://www.facebook.com" target="_blank" rel="noopener noreferrer" className="icon">
              <FontAwesomeIcon icon={faFacebook} />
            </a>
            <a href="https://www.twitter.com" target="_blank" rel="noopener noreferrer" className="icon">
              <FontAwesomeIcon icon={faTwitter} />
            </a>
            <a href="https://www.instagram.com" target="_blank" rel="noopener noreferrer" className="icon">
              <FontAwesomeIcon icon={faInstagram} />
            </a>
          </div>
        </div>
        
      </div>
      </div> 

      {/* copy right */}
      <div className="mt-3 w-100 text-center" style={{color:'white'}} >
        <hr />
          <p style={{fontSize:"0.75rem"}} className="mx-auto">Copyright ©2023 | All Right Reserved</p>
        <br/>
      </div>

    </div>
  )
}