import React, { useEffect, useState } from "react";
import "./slider.css";

import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation, Pagination } from 'swiper';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css';

const Slider = () => {
  const [books, setBooks] = useState([]);
  const getBooks = async () => {
    try {
      const response = await fetch("http://localhost:5000/books");
      const jsonData = await response.json();
      setBooks(jsonData);
    } catch (err) {
      console.error(err.message);
    }
  };

  useEffect(() => {
    getBooks();
  }, []);

  console.log(books);

  return (
    <div className="container">
        <Swiper
        modules={[Pagination, Navigation]}
        className="mySwiper"
        navigation={true}
        loopFillGroupWithBlank={true}
        slidesPerView={3} spaceBetween={40} slidesPerGroup={1} loop={true}
            >
            {books.map(book => (
                <SwiperSlide>
                    <div className="left">
                            <div className="pName">
                                <span>{book.name}</span>
                                <span>{book.author}</span>
                            </div>
                            <span>{book.price}</span>
                            <div className="shop">Show Now</div>
                    </div>
                    <div>
                        {book.image && (
                            <img src={`http://localhost:5000/image/${book.image}`} alt="No product" className="iproduct" 
                            />
                            )}
                    </div>
                </SwiperSlide>
                ))}                  
        </Swiper> 
    </div>
  )
}

export default Slider;