import React, { useState } from 'react';
import css from "./Product.module.css";
import { dataSlider } from './data';
import { useAutoAnimate } from '@formkit/auto-animate/react';

const Product = () => {
  const [parent] = useAutoAnimate()
  const [MenuProducts, setMenuProducts] = useState(dataSlider);

  const filter = (type) => {
    setMenuProducts(dataSlider.filter((product)=>product.type === type))
  }
  return (
    <div className={css.container}>
      <h1>Our feature Products</h1>

      <div className={css.products}>
          <ul className={css.menu}>
              <li onClick={()=>setMenuProducts(dataSlider)}>ALL</li>
              <li onClick={()=>filter("Romance")}>Romance</li>
              <li onClick={()=>filter("SciFi")}>SciFi</li>
          </ul>

          <div className={css.list} ref={parent}>
              { MenuProducts.map((product, i)=>(
                <div className={css.product}>
                  <div className="left">
                    <div className="name">
                      <span>{product.name}</span>
                      <span>{product.description}</span>
                    </div>
                    <span>${product.price}</span>
                    <div>Show Now</div>
                  </div>

                  <img src={product.img} alt='' className={css.img} />
                </div>
              ))}
          </div>
      </div>
    </div>
  )
}

export default Product;