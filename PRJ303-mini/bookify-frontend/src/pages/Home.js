import React from  'react';
import "./Home.css";
import Slider from '../components/slider/slider';
import HeroSlider from '../components/hero/HeroSlider';
import Header from "../components/header/header";
import Footer from "../components/footer/footer";
import Filter from './Filter';

const Home2 = () => {
  return (
    <>
      <Header />
      <HeroSlider />
      <Filter />
      <Footer />
    </>
  );
};

export default Home2;
