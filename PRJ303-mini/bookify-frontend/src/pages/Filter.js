import React, { useState, useEffect } from 'react';
import css from "./Product.module.css";
import "./productCard.css";
import styled from 'styled-components';

const Button = styled.button`
  background-color: #601A49;
  color: white;
  padding: 10px 20px;
  font-size: 14px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  height: 36px; 
  width: 120px; 

  &:hover {
    background-color: #ce1992;
  }

  @media (max-width: 768px) {
    font-size: 14px;
    padding: 0 16px;
  }

  @media (max-width: 769px) {
    font-size: 14px;
    padding: 0 16px;
  }

  @media (max-width: 500px) {
    font-size: 14px;
    width: 100%;
  }
`;

const Search = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #601A49;
  color:  #ffffff;
  padding: 0px 20px;
  font-size: 16px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  height: 40px;
  width: 72px;

  &:hover {
    color: #ffffff;
    background-color: #ce1992;
  }

  @media (max-width: 768px) {
    font-size: 18px;
    padding: 0 16px;
  }
`;


const Filter = () => {
  const [books, setBooks] = useState([]);
  const [MenuProducts, setMenuProducts] = useState([]);
  const [showAll, setShowAll] = useState(true);
  const [show, setShow] = useState(false);
  const [modalBook, setModalBook] = useState(null);
  const [searchTerm, setSearchTerm] = useState('');
  const [showSuccessMessage, setShowSuccessMessage] = useState(false);

  const handleShow = (book) => {
    setModalBook(book);
    setShow(true);
  };

  const filter = (type) => {
    if (type === "all") {
      setMenuProducts(books);
      setShowAll(true);
    } else {
      setMenuProducts(books.filter((book) => book.type === type).filter(filterBySearchTerm));
      setShowAll(false);
    }
  };

  const filterBySearchTerm = (book) => {
    const title = book.name.toLowerCase();
    const search = searchTerm.toLowerCase();
    return title.includes(search);
  };

  const handleSearch = () => {
    const filteredBooks = books.filter(filterBySearchTerm);
    setMenuProducts(filteredBooks);
    setShowAll(filteredBooks.length === 0);
  };

  const getBooks = async () => {
    try {
      const response = await fetch("http://localhost:5000/books");
      const jsonData = await response.json();
      setBooks(jsonData);
      setMenuProducts(jsonData);
    } catch (err) {
      console.error(err.message);
    }
  };

  useEffect(() => {
    getBooks();
  }, []);

  return (
    <div className={css.container}>
      <h1>Our Feature Products</h1>

      <div className={css.products}>
          <ul className={`${css.menu} ${css.vertical}`}>
          <li onClick={() => filter("all")}>ALL</li>
          <li onClick={() => filter("arts")}>Arts</li>
          <li onClick={() => filter("crime")}>Crime</li>
          <li onClick={() => filter("biography")}>Biography</li>
          <li onClick={() => filter("romance")}>Romance</li>
          <li onClick={() => filter("thriller")}>Thriller</li>
          <li onClick={() => filter("fiction")}>Fiction</li>
          <li onClick={() => filter("movies")}>Movies</li>
          <li onClick={() => filter("religious")}>Religious</li>
          <li onClick={() => filter("novel")}>Novel</li>
          
              <div className={css.search}>
                <input
                  type="text"
                  placeholder="Search by title"
                  value={searchTerm}
                  style={{ height: '40px', width: '160px' }}
                  onChange={(e) => setSearchTerm(e.target.value)}
                />
                <Search onClick={handleSearch}>
                  Search
                </Search>
              </div>
          </ul>
      </div>

      <div className={css.products2}>
        <div className="product_container2 container-fluid" >
          <div className="grid d-flex justify-content-center">
            <div id="wrap">
              <div id="columns" className="columns_4" >
                {MenuProducts.length > 0 ? (
                  MenuProducts.map((book) => (
                    <figure key={book.id} className={css.figure}>
                      <img
                        src={`http://localhost:5000/image/${book.image}`}
                        alt="No Image"
                        className={css.image}
                      />
                      <div className={css.title}>Title: {book.name}</div>
                      <div className={css.details}>Author: {book.author}</div>
                      <div className={css.details}>Price: {book.price}</div>
                      
                        <Button
                          type="button"
                          data-toggle="modal"
                          data-target="#myModal"
                          onClick={() => handleShow(book)}
                        >
                          View Details
                        </Button>
                    </figure>
                  ))
                ) : (
                  <div className={css.notFound}>No books found.</div>
                )}
              </div>
            </div>
          </div>
        </div>

      </div>
      {show && modalBook && (
        <div className="container">
          <div className="modal fade" id="myModal" role="dialog">
            <div className="modal-dialog modal-lg"> {/* Added modal-lg class */}
              <div className="modal-content">
                <div className="modal-header">
                  <button type="button" className="close" data-dismiss="modal">
                    &times;
                  </button>
                </div>

                <div className="modal-body">
                  <div>
                    <img src={`http://localhost:5000/image/${modalBook.image}`} alt="No Image" className="modal-image" style={{maxWidth:'400px',maxHeight:'300px'}}/>
                  </div>
                  <div>
                    <span className="title">Title: {modalBook.name}</span>
                  </div>
                  <div>
                    <span className="author">Author: {modalBook.author}</span>
                  </div>
                  <div>
                    <span className="price">Price: Nu. {modalBook.price}</span>
                  </div>
                  <br />
                  <div>
                    <span className="description">Description: <br />{modalBook.description}</span>
                  </div>
                </div>

                <div className="modal-footer">
                  <Button type="button" data-dismiss="modal">close</Button>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default Filter;
