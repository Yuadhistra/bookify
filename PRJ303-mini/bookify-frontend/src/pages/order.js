import React, { Fragment, useState, useEffect } from 'react';
import css from "./contact.module.css";
import Header from '../components/header/header';
import Footer from '../components/footer/footer';
import styled from 'styled-components';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Remove = styled.button`
  background-color:  #F67280;
  color: white;
  padding: 0.5rem 1rem;
  font-size: 1rem;
  border: none;
  border-radius: 0.25rem;
  margin-top: 0.75rem;
  cursor: pointer;

  &:hover {
    background-color: #D2042D;
  }

  @media (max-width: 768px) {
    font-size: 0.9rem;
    margin-top: 0.5rem;
  }

  @media (max-width: 480px) {
    font-size: 0.8rem;
    margin-top: 0.5rem;
  }
`;

const Button = styled.button`
  background-color: #4d9b0e;
  color: white;
  padding: 10px 20px;
  font-size: 14px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  height: 36px; 
  width: 120px; 

  &:hover {
    background-color: #47e43e;
  }

  @media (max-width: 768px) {
    font-size: 14px;
    padding: 0 16px;
  }

  @media (max-width: 769px) {
    font-size: 14px;
    padding: 0 16px;
  }

  @media (max-width: 500px) {
    font-size: 14px;
    width: 100%;
  }
`;

const Add = styled.button`
  background-color: #4d9b0e;
  color: white;
  padding: 0.5rem 1rem;
  font-size: 1rem;
  border: none;
  border-radius: 0.25rem;
  margin-top: 0.75rem;
  cursor: pointer;

  &:hover {
    background-color: #47e43e;
  }

  @media (max-width: 768px) {
    font-size: 0.9rem;
    margin-top: 0.5rem;
  }

  @media (max-width: 480px) {
    font-size: 0.8rem;
    margin-top: 0.5rem;
  }
`;

const Heading2 = styled.h1`
  text-align: center;
  margin-top: 5rem;
  font-size: 2.5rem;

  @media (max-width: 768px) {
    font-size: 2rem;
    margin-top: 3rem;
  }

  @media (max-width: 480px) {
    font-size: 1.5rem;
    margin-top: 2rem;
  }
`;

const DeleteButton = styled.button`
  background-color: #F67280;
  color: white;
  padding: 10px 20px;
  font-size: 14px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  height: 36px; 
  width: 120px; 

  &:hover {
    background-color: #D2042D;
  }

  @media (max-width: 768px) {
    font-size: 14px;
    padding: 0 16px;
  }

  @media (max-width: 769px) {
    font-size: 14px;
    padding: 0 16px;
  }

  @media (max-width: 500px) {
    font-size: 14px;
    width: 100%;
  }
`;
const Order = () => {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [address, setAddress] = useState("");
  const [books, setBooks] = useState([{ title: '', author: '', quantity: '', price: '' }]);
  const [errors, setErrors] = useState({});
  const [total, setTotal] = useState(0);

  useEffect(() => {
    calculateTotal();
  }, [books]);
  
  const removeBookField = (index) => {
    const updatedBooks = [...books];
    updatedBooks.splice(index, 1);
    setBooks(updatedBooks);
  };

  const calculateTotal = () => {
    let sum = 0;
    books.forEach((book) => {
      const quantity = parseInt(book.quantity);
      const price = parseFloat(book.price);
      if (!isNaN(quantity) && !isNaN(price)) {
        sum += quantity * price;
      }
    });
    setTotal(sum.toFixed(2));
  };

  const validateForm = () => {
    const errors = {};
    let isValid = true;

    if (!username.trim()) {
      errors.username = "Username is required";
      isValid = false;
    }

    if (!email.trim()) {
      errors.email = "Email is required";
      isValid = false;
    } else if (!/\S+@\S+\.\S+/.test(email)) {
      errors.email = "Invalid email format";
      isValid = false;
    }

    if (!address.trim()) {
      errors.address = "Address is required";
      isValid = false;
    }

    setErrors(errors);
    return isValid;
  };

  const handleBookFieldChange = (index, field, value) => {
    const updatedBooks = [...books];
    updatedBooks[index][field] = value;
    setBooks(updatedBooks);
  };

  const addBookField = () => {
    setBooks([...books, { title: '', author: '', quantity: '', price: '' }]);
  };

  const bookdetails = books.map((book) => {
    return `title: ${book.title}, author: ${book.author}, price: ${book.price}`;
  });

  const onSubmitForm = async (e) => {
    e.preventDefault();
    if (validateForm()) {
      try {
        
        const body = { username,email,address, bookdetails, total };
        const response = await fetch("http://localhost:5000/order", {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify(body),
        });
        toast.success('Book delete Successfully.')
        setTimeout(() => {
        window.location = "/order";
      }, 2000);  
      } catch (err) {
        console.error(err.message);
      }
    }
  };

  return (
    <section>
      <Header />
      <section className={css.contact_herobg2}>
      </section>
      <Fragment>
      <ToastContainer position='top-center' />
        <Heading2>Place your order!</Heading2>
        <form className="mt-5" onSubmit={onSubmitForm}>
          <div className="container">
            <div className="row">
              <div className="col-md-6 offset-md-3">
                <div className="form-group">
                  <label htmlFor="username">Username</label>
                  <input
                    type="text"
                    className={`form-control ${errors.username && 'is-invalid'}`}
                    id="username"
                    value={username}
                    onChange={(e) => setUsername(e.target.value)}
                  />
                  {errors.username && <div className="invalid-feedback">{errors.username}</div>}
                </div>
                <div className="form-group">
                  <label htmlFor="email">Email</label>
                  <input
                    type="text"
                    className={`form-control ${errors.email && 'is-invalid'}`}
                    id="email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                  />
                  {errors.email && <div className="invalid-feedback">{errors.email}</div>}
                </div>
                <div className="form-group">
                  <label htmlFor="address">Address</label>
                  <input
                    type="text"
                    className={`form-control ${errors.address && 'is-invalid'}`}
                    id="address"
                    value={address}
                    onChange={(e) => setAddress(e.target.value)}
                  />
                  {errors.address && <div className="invalid-feedback">{errors.address}</div>}
                </div>

                <div className="form-group">
              <label>Book Details</label>
              {books.map((book, index) => (
                <div key={index}>
                  {/* Title Field */}
                  <div className="form-group">
                    <label htmlFor={`title-${index}`}>Title</label>
                    <input
                      type="text"
                      className={`form-control ${errors.title && 'is-invalid'}`}
                      id={`title-${index}`}
                      value={book.title}
                      onChange={(e) => handleBookFieldChange(index, 'title', e.target.value)}
                    />
                    {errors.title && <div className="invalid-feedback">{errors.title}</div>}
                  </div>
                  {/* Author, Quantity, and Price Fields */}
                  <div className="form-row">
                    <div className="col">
                      <label htmlFor={`author-${index}`}>Author</label>
                      <input
                        type="text"
                        className={`form-control ${errors.author && 'is-invalid'}`}
                        id={`author-${index}`}
                        value={book.author}
                        onChange={(e) => handleBookFieldChange(index, 'author', e.target.value)}
                      />
                    </div>
                    <div className="col">
                      <label htmlFor={`quantity-${index}`}>Quantity</label>
                      <input
                        type="number"
                        className={`form-control ${errors.quantity && 'is-invalid'}`}
                        id={`quantity-${index}`}
                        value={book.quantity}
                        onChange={(e) => handleBookFieldChange(index, 'quantity', e.target.value)}
                      />
                    </div>
                    <div className="col">
                      <label htmlFor={`price-${index}`}>Price</label>
                      <input
                        type="number"
                        className={`form-control ${errors.price && 'is-invalid'}`}
                        id={`price-${index}`}
                        value={book.price}
                        onChange={(e) => handleBookFieldChange(index, 'price', e.target.value)}
                      />
                    </div>
                  </div>
                  {books.length > 1 && (
                    <Remove type="button" onClick={() => removeBookField(index)}>
                      Remove Book
                    </Remove>
                  )}
                  {errors.author && <div className="invalid-feedback">{errors.author}</div>}
                  {errors.quantity && <div className="invalid-feedback">{errors.quantity}</div>}
                  {errors.price && <div className="invalid-feedback">{errors.price}</div>}
                </div>
              ))}
              <Add type="button"  onClick={addBookField}>
                Add Another Book
              </Add>
            </div>

            <div className="form-group">
              <label htmlFor="total">Total</label>
              <input
                type="text"
                className="form-control"
                id="total"
                value={`$${total}`}
                readOnly
              />
            </div>
            <Button type="submit" >
              Submit
            </Button>
          </div>
        </div>
      </div>
    </form>
  </Fragment>
  <Footer />
</section>
);
};

export default Order;

