import React, { Fragment, useState } from 'react';
import css from "./contact.module.css";
import Header from '../components/header/header';
import Footer from '../components/footer/footer';
import styled from 'styled-components';

const Heading = styled.h1`
  text-align: center;
  margin-top: 2rem;
  font-size: 18px;
  font-weight: bold;
  color: black;

  @media (max-width: 768px) {
    font-size: 2rem;
  }

  @media (max-width: 480px) {
    font-size: 1.5rem;
  }
`;

const Contact = () => {
  const [username, setUsername] = useState("");
  const [title, setTitle] = useState("");
  const [email, setEmail] = useState("");
  const [description, setDescription] = useState("");
  const [errors, setErrors] = useState({});

  const validateForm = () => {
    const errors = {};
    let isValid = true;

    if (!username.trim()) {
      errors.username = "Username is required";
      isValid = false;
    }

    if (!title.trim()) {
      errors.title = "Title is required";
      isValid = false;
    }

    if (!email.trim()) {
      errors.email = "Email is required";
      isValid = false;
    } else if (!/\S+@\S+\.\S+/.test(email)) {
      errors.email = "Invalid email format";
      isValid = false;
    }

    if (!description.trim()) {
      errors.description = "Description is required";
      isValid = false;
    }

    setErrors(errors);
    return isValid;
  };

  const onSubmitForm = async (e) => {
    e.preventDefault();
    if (validateForm()) {
      try {
        const body = { username, title, email, description };
        const response = await fetch("http://localhost:5000/feedback", {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify(body),
        });
        window.location = "/contact";
      } catch (err) {
        console.error(err.message);
      }
    }
  };

  return (
    <section>
    <Header />
      <section className={css.contact_herobg}>
      <Heading >Contact Us</Heading>
      </section>
        <Fragment>
      <h1 className="text-center mt-5">Give Feedback</h1>
      <form className="mt-5" onSubmit={onSubmitForm}>
        <div className="container">
          <div className="row">
            <div className="col-md-6 offset-md-3">
              <div className="form-group">
                <label htmlFor="title">Title</label>
                <input
                  type="text"
                  className={`form-control ${errors.title && 'is-invalid'}`}
                  id="title"
                  value={title}
                  onChange={(e) => setTitle(e.target.value)}
                />
                {errors.title && <div className="invalid-feedback">{errors.title}</div>}
              </div>
              <div className="form-group">
                <label htmlFor="username">Username</label>
                <input
                  type="text"
                  className={`form-control ${errors.username && 'is-invalid'}`}
                  id="username"
                  value={username}
                  onChange={(e) => setUsername(e.target.value)}
                />
                {errors.username && <div className="invalid-feedback">{errors.username}</div>}
              </div>
              <div className="form-group">
                <label htmlFor="email">Email</label>
                <input
                  type="text"
                  className={`form-control ${errors.email && 'is-invalid'}`}
                  id="email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
                {errors.email && <div className="invalid-feedback">{errors.email}</div>}
              </div>
              <div className="form-group">
                <label htmlFor="description">Description</label>
                <textarea
                  className={`form-control ${errors.description && 'is-invalid'}`}
                  id="description"
                  rows="3"
                  value={description}
                  onChange={(e) => setDescription(e.target.value)}
                ></textarea>
                {errors.description && <div className="invalid-feedback">{errors.description}</div>}
              </div>
              <button type="submit" className="btn btn-success">
                Add
              </button>
            </div>
          </div>
        </div>
      </form>
    </Fragment>
      <Footer />
    </section>
  );
};

export default Contact; 