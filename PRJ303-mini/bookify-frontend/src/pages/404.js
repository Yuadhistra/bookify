import React from "react";
import { Link } from "react-router-dom";
import css from "./notfound.module.css"

const Notfound = () => {
  return (
    <div className={css.container}>
      <div className={css.container2}>
        <h2 className={css.text}>
          <span className={css.text2}>Oops!</span> 404 | Something went
          wrong
        </h2>
        <Link
          to="/"
          className={css.btn}
        >
          &larr; Go to Login
        </Link>
      </div>
    </div>
  );
};

export default Notfound;
