import React, { useState, useEffect } from 'react';
import "./productCard.css";

const ProductContainer = () => {
  const [books, setBooks] = useState([]);
  const [show, setShow] = useState(false);
  const [modalBook, setModalBook] = useState(null);

  const handleShow = (book) => {
    setModalBook(book);
    setShow(true);
  };

  const getBooks = async () => {
    try {
      const response = await fetch("http://localhost:5000/books");
      const jsonData = await response.json();
      setBooks(jsonData);
    } catch (err) {
      console.error(err.message);
    }
  };

  useEffect(() => {
    getBooks();
  }, []);

  return (
    <>
      <div className="product-container">
        <div className="grid grid-cols-3 gap-10 w-[80%] mx-auto pb-20">
          <div id="wrap">
            <div id="columns" class="columns_4">
              {books.map((book) => (
                <figure key={book.id}>
                  <img src={`http://localhost:5000/image/${book.image}`} alt="No Image" />
                  <div className='title'>Title: {book.name}</div>
                  <div className='details'>Author: {book.author}</div>
                  <div className='details'>Price: {book.price}</div>
                  <button
                    type="button"
                    className="btn btn-info btn-lg"
                    data-toggle="modal"
                    data-target="#myModal"
                    onClick={() => handleShow(book)}
                  >
                    Open Modal
                  </button>
                </figure>
              ))}
            </div>
          </div>
        </div>
      </div>

      {show && modalBook && (
        <div className="container">
          <div className="modal fade" id="myModal" role="dialog">
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <button type="button" className="close" data-dismiss="modal">&times;</button>
                </div>
                <div className="modal-body">
                  <div>
                    <span className="price">Nu.{modalBook.price}</span>
                  </div>
                </div>
                <div className="modal-footer">
                  <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
}

export default ProductContainer;
