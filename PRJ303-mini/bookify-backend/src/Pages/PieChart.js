import React, { useState, useEffect } from 'react';
import {
  AccumulationChartComponent,
  AccumulationSeriesCollectionDirective,
  AccumulationSeriesDirective,
  Inject,
  AccumulationDataLabel,
  AccumulationTooltip,
  PieSeries
} from '@syncfusion/ej2-react-charts';

function PieChart() {
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch('http://localhost:5000/books')
      .then(response => response.json())
      .then(data => {
        const bookTypes = {};
        let totalBooks = 0;

        data.forEach(book => {
          if (book.type in bookTypes) {
            bookTypes[book.type]++;
          } else {
            bookTypes[book.type] = 1;
          }
          totalBooks++;
        });

        const chartData = Object.keys(bookTypes).map(type => ({
          x: type,
          y: bookTypes[type],
          text: `${bookTypes[type]}`
        }));

        setData(chartData);
      })
      .catch(error => {
        console.error('Error fetching data:', error);
      });
  }, []);

  const datalabel = { visible: true, position: 'Inside', name: 'text' };
  const tooltip = { enable: true };
  const tooltipRender = args => {
    let value = (args.point.y / args.series.sumOfPoints) * 100;
    args.text = args.point.x + ' ' + Math.ceil(value) + '%';
  };

  return (
    <AccumulationChartComponent
      id='charts'
      tooltip={tooltip}
      // title='Book statics'
      tooltipRender={tooltipRender}
    >
      <Inject services={[AccumulationTooltip, PieSeries, AccumulationDataLabel]} />
      <AccumulationSeriesCollectionDirective>
        <AccumulationSeriesDirective
          dataSource={data}
          xName='x'
          yName='y'
          radius='70%'
          dataLabel={datalabel}
        />
      </AccumulationSeriesCollectionDirective>
    </AccumulationChartComponent>
  );
}

export default PieChart;
