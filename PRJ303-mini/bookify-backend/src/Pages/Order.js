import React from 'react';
import styled from 'styled-components';
import Sidebar from '../components/sidebar/Sidebar';
import ListuserOrder from '../components/userOrder';

const GridContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(1, 1fr);
  grid-gap: 1rem;
  margin-top: 1rem;
  margin-left: 1rem;
`;

const Order = () => {
  return (
    <div className='home'>
      <Sidebar />
      <div className='homecontainer'>
        <GridContainer>
          <div className="item1">
            <ListuserOrder />
          </div>
        </GridContainer>
      </div>
    </div>
  )
}

export default Order;