import React, { useEffect, useState } from 'react';
import Sidebar from '../components/sidebar/Sidebar';
import './home.scss';
import PieChart from './PieChart';
import HeroSlider from './HeroSlider';

const Home = () => {
  const [totalUsers, setTotalUsers] = useState(0);
  const [totalBooks, setTotalBooks] = useState(0);
  const [totalFeedback, setTotalFeedback] = useState(0);
  const [adminCount, setAdminCount] = useState(0);

  useEffect(() => {
    fetchTotalUsers();
    fetchTotalBooks();
    fetchTotalFeedback();
    fetchAdminCount();
  }, []);

  const fetchTotalUsers = async () => {
    try {
      const response = await fetch('http://localhost:5000/userdetails');
      const data = await response.json();
      setTotalUsers(data.length);
    } catch (error) {
      console.error(error.message);
    }
  };

  const fetchTotalBooks = async () => {
    try {
      const response = await fetch('http://localhost:5000/books');
      const data = await response.json();
      setTotalBooks(data.length);
    } catch (error) {
      console.error(error.message);
    }
  };

  const fetchTotalFeedback = async () => {
    try {
      const response = await fetch('http://localhost:5000/feedback');
      const data = await response.json();
      setTotalFeedback(data.length);
    } catch (error) {
      console.error(error.message);
    }
  };

  const fetchAdminCount = async () => {
    try {
      const response = await fetch('http://localhost:5000/adminsignup');
      const data = await response.json();
      setAdminCount(data.length);
    } catch (error) {
      console.error(error.message);
    }
  };

  return (
    <div className="home">
      <Sidebar /> 
      <div className="homecontainer">
        <h2>DASHBOARD</h2>
        <div>
          <HeroSlider />
        </div>
        <div class="flex-container">
            <div class="column">
              <div class="content-container">
                  <p>Total Users</p>
                  <p>{totalUsers}</p>
              </div>
              <div class="content-container">
                <p>Book Statics</p>
                  <PieChart />
              </div>
            </div>

            <div class="column">
              <div class="content-container">
                  <p>Total Feedback</p>
                  <p>{totalFeedback}</p>
              </div>
              <div class="content-container">
                  <p>Admin</p>
                  <p>{adminCount}</p>
              </div>
            </div>
        </div>

      </div>
    </div>
  );
};

export default Home;

