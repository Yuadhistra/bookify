import React from 'react'
import Sidebar from '../components/sidebar/Sidebar'
import UserDetails from '../components/userDetails';
import styled from 'styled-components';

const GridContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(1, 1fr);
  grid-gap: 1rem;
  margin-top: 1rem;
  margin-left: 1rem;
`;

const User = () => {
  return (
    <div className='home'>
      <Sidebar />
        <div className='homecontainer'>
            <GridContainer>
              <div className="item1">
                <UserDetails />
              </div>
            </GridContainer>
        </div>
    </div>
  )
}

export default User