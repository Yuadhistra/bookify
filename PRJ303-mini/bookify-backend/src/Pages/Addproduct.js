import React from 'react';
import styled from 'styled-components';
import Sidebar from '../components/sidebar/Sidebar';
import InputBook from '../components/InputBook';

const GridContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(1, 1fr);
  grid-gap: 1rem;
  margin-top: 1rem;
  margin-left: 1rem;
`;

const Addproduct = () => {
  return (
    <div className='home'>
      <Sidebar />
      <div className='homecontainer'>
        <GridContainer>
          <div className="item1">
            <InputBook />
          </div>
        </GridContainer>
      </div>
    </div>
  )
}

export default Addproduct;