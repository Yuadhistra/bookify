import React, { useState, useEffect } from 'react';
import css from "./Product.module.css";
import "./productCard.css";

const Filter = () => {
  const [books, setBooks] = useState([]);
  const [MenuProducts, setMenuProducts] = useState([]);
  const [showAll, setShowAll] = useState(true); // Add showAll state
  const [show, setShow] = useState(false);
  const [modalBook, setModalBook] = useState(null);
  const [searchTerm, setSearchTerm] = useState('');

  const handleShow = (book) => {
    setModalBook(book);
    setShow(true);
  };

  const filter = (type) => {
    if (type === "all") {
      setMenuProducts(books);
      setShowAll(true);
    } else {
      setMenuProducts(books.filter((book) => book.type === type).filter(filterBySearchTerm));
      setShowAll(false); 
    }
  };

    const filterBySearchTerm = (book) => {
        const title = book.name.toLowerCase();
        const author = book.author.toLowerCase();
        const search = searchTerm.toLowerCase();
        return title.includes(search) || author.includes(search);
    };

    const handleSearch = () => {
      const filteredBooks = books.filter(filterBySearchTerm);
      setMenuProducts(filteredBooks);
      setShowAll(filteredBooks.length === 0);
    };

  const getBooks = async () => {
    try {
      const response = await fetch("http://localhost:5000/books");
      const jsonData = await response.json();
      setBooks(jsonData);
      setMenuProducts(jsonData);
    } catch (err) {
      console.error(err.message);
    }
  };

  useEffect(() => {
    getBooks();
  }, []);

  return (
    <div className={css.container}>
      <h1>Our feature Products</h1>

      <div className={css.products}>
        <ul className={css.menu}>
        <li onClick={() => filter("all")}>ALL</li>
          <li onClick={() => filter("arts")}>Arts</li>
          <li onClick={() => filter("crime")}>Crime</li>
          <li onClick={() => filter("biography")}>Biography</li>
          <li onClick={() => filter("romance")}>Romance</li>
          <li onClick={() => filter("thriller")}>Thriller</li>
          <li onClick={() => filter("fiction")}>Fiction</li>
          <li onClick={() => filter("movies")}>Movies</li>
          <li onClick={() => filter("religious")}>Religious</li>

          <div className={css.search}>
            <input
                type="text"
                placeholder="Search by title"
                value={searchTerm}
                onChange={(e) => setSearchTerm(e.target.value)}
            />
            <button className={css.searchButton} onClick={handleSearch}>
                Search
            </button>
        </div>
        </ul>

        <div className="product_container">
          <div className="grid grid-cols-3 gap-10 w-[80%] mx-auto pb-20">
            <div id="wrap">
              <div id="columns" className="columns_4">
                {MenuProducts.length > 0 ? (
                  MenuProducts.map((book) => (
                    <figure key={book.id}>
                      <img src={`http://localhost:5000/image/${book.image}`} alt="Not Found" />
                      <div className='title'>Title: {book.name}</div>
                      <div className='details'>Author: {book.author}</div>
                      <div className='details'>Price: {book.price}</div>
                      <button
                        type="button"
                        className="btn btn-info btn-lg"
                        data-toggle="modal"
                        data-target="#myModal"
                        onClick={() => handleShow(book)}
                      >
                        Open Modal
                      </button>
                    </figure>
                  ))
                ) : (
                  <div className={css.notFound}>No books found.</div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>

      {show && modalBook && (
        <div className="container">
          <div className="modal fade" id="myModal" role="dialog">
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <button type="button" className="close" data-dismiss="modal">&times;</button>
                </div>
                <div className="modal-body">
                  <div>
                    <span className="price">Nu.{modalBook.price}</span>
                  </div>
                </div>
                <div className="modal-footer">
                  <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

export default Filter;