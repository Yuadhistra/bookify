import React from 'react';
import styled from 'styled-components';
import Sidebar from '../components/sidebar/Sidebar';
import ListBook from '../components/ListBook';

const GridContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(1, 1fr);
  grid-gap: 1rem;
  margin-top: 1rem;
  margin-left: 1rem;
`;

const Updateproduct = () => {
  return (
    <div className='home'>
      <Sidebar />
      <div className='homecontainer'>
        <GridContainer>
          <div className="item1">
            <ListBook />
          </div>
        </GridContainer>
      </div>
    </div>
  )
}

export default Updateproduct;