import { Route, Routes } from "react-router-dom";
import Home from "./Pages/Home";
import Addproduct from "./Pages/Addproduct";
import User from "./Pages/User";
import Feedback from "./Pages/Feedback";
import Updateproduct from "./Pages/Updateproduct";
// import RegistrationForm from "./sign/Registration";
import Order from "./Pages/Order";

import LoginForm from './sign/Login';
import Filter from "./Pages/Filter";

function App() {

  return (
    <div className="app">
        <div style={{width:'100%', height:'3rem'}} />
        <Routes>
          <Route path="/">
            <Route index element={<LoginForm />} />
            <Route path="/login" element={<LoginForm />}/>
            {/* <Route path="/register" element={<RegistrationForm />}/> */}
            <Route path="/home" element={<Home/>}/>
            <Route path="/product" element={<Addproduct/>} />
            <Route path="/update" element={<Updateproduct/>} />
            <Route path="/order" element={<Order/>} />
            <Route path="/feedback" element={<Feedback />} />  
            <Route path ="/user" element={<User />} />
          </Route>
        </Routes>     
    </div>
  );
}

export default App;
