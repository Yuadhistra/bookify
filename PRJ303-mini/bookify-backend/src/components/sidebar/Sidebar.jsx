import "./sidebar.scss"
import React from 'react';
import { Link } from 'react-router-dom';
import DashboardIcon from '@mui/icons-material/Dashboard';
import PersonOutlineOutlinedIcon from '@mui/icons-material/PersonOutlineOutlined';
import PostAddIcon from '@mui/icons-material/PostAdd';
import MailOutlineIcon from '@mui/icons-material/MailOutline';
import LogoutIcon from '@mui/icons-material/Logout';
import logo from '../Assets/logo.png';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Sidebar = () => {
  const handleLogout = () => {
    toast.success('Logged out successfully.');
    setTimeout(() => {
      window.location = "/login";
    }, 2000);
  };

  return (
    <div className="sidebar">
      <div className="top">
        <img src={logo} className="logo" alt="logo" />
      </div>
      <div className="center">
        <ul>
          <p className="titles">MAIN</p>
          <Link to="/home" style={{ textDecoration: 'none' }}>
            <li>
              <DashboardIcon className="icon" />
              <span>Dashboard</span>
            </li>
          </Link>
          <p className="titles">DETAILS</p>
          <Link to="/user" style={{ textDecoration: 'none' }}>
            <li>
              <PersonOutlineOutlinedIcon className="icon" />
              <span>Users</span>
            </li>
          </Link>
          <Link to="/product" style={{ textDecoration: 'none' }}>
            <li>
              <PostAddIcon className="icon" />
              <span>Add Books</span>
            </li>
          </Link>
          <Link to="/update" style={{ textDecoration: 'none' }}>
            <li>
              <PostAddIcon className="icon" />
              <span>Updates Books</span>
            </li>
          </Link>
          <Link to="/order" style={{ textDecoration: 'none' }}>
            <li>
              <PostAddIcon className="icon" />
              <span>User Orders</span>
            </li>
          </Link>
          <Link to="/feedback" style={{ textDecoration: 'none' }}>
            <li>
              <MailOutlineIcon className="icon" />
              <span>Feedback</span>
            </li>
          </Link>
          <Link to="/login" style={{ textDecoration: 'none' }}>
            <li onClick={handleLogout}>
              <LogoutIcon className="icon" />
              <span>Log Out</span>
            </li>
          </Link>
        </ul>
      </div>
    </div>
  );
};

export default Sidebar;
