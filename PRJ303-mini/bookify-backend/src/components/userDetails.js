import React, { Fragment, useEffect, useState } from 'react';
import styles from './userDetails.module.css';
import styled from 'styled-components';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const DeleteButton = styled.button`
  background-color: #F67280;
  color: white;
  padding: 10px 20px;
  font-size: 14px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  height: 36px; 
  width: 120px; 

  &:hover {
    background-color: #D2042D;
  }

  @media (max-width: 768px) {
    font-size: 14px;
    padding: 0 16px;
  }

  @media (max-width: 769px) {
    font-size: 14px;
    padding: 0 16px;
  }

  @media (max-width: 500px) {
    font-size: 14px;
    width: 100%;
  }
`;

const UserDetails = () => {
    const [users, setUsers] = useState([]);

    const deleteUser = async (id) => {
        try {
            await fetch(`http://localhost:5000/userdetails/${id}`, {
                method: 'DELETE'
            });

            setUsers(users.filter((user) => user.email_id !== id));
            toast.success('user details deleted Successfully.')
            setTimeout(()=>{
              window.location = '/user';
            },2000);
        } catch (error) {
            console.error(error.message);
        }
    };

    const getUsers = async () => {
        try {
            const response = await fetch('http://localhost:5000/userdetails');
            const data = await response.json();
            setUsers(data);
        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        getUsers();
    }, []);

    console.log(users);

    return (
        <Fragment>
            <ToastContainer position='top-center'/>
            <h1 className='text-center mt-5'>User Details Table</h1>
            <table className={`${styles.table} mt-5`}>
                <thead>
                    <tr>
                    <th className={styles.tableHeader}>Email</th>
                    <th className={styles.tableHeader}>Username</th>
                    <th className={styles.tableHeader}>Phone Number</th>
                    <th className={styles.tableHeader}>Password</th>
                    <th className={styles.tableHeader}>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {users.map((user) => (
                    <tr key={user.email_id}>
                        
                        <td className={styles.tableData}>{user.email}</td>
                        <td className={styles.tableData}>{user.first_name}</td>
                        <td className={styles.tableData}>{user.phonenumber}</td>
                        <td className={styles.tableData}>{user.hash_password}</td>
                        <td className={styles.tableData}>
                        <DeleteButton onClick={() => deleteUser(user.email_id)} >
                            Delete
                        </DeleteButton>
                        </td>
                    </tr>
                    ))}
                </tbody>
            </table>
        </Fragment>
    );
};

export default UserDetails;
