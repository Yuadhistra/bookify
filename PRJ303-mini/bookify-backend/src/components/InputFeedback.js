import React, { Fragment, useState } from 'react';

const InputFeedback = () => {
  const [username, setUsername] = useState("");
  const [title, setTitle] = useState("");
  const [email, setEmail] = useState("");
  const [description, setDescription] = useState("");
  const [errors, setErrors] = useState({});

  const validateForm = () => {
    const errors = {};
    let isValid = true;

    if (!username.trim()) {
      errors.username = "Username is required";
      isValid = false;
    }

    if (!title.trim()) {
      errors.title = "Title is required";
      isValid = false;
    }

    if (!email.trim()) {
      errors.email = "Email is required";
      isValid = false;
    } else if (!/\S+@\S+\.\S+/.test(email)) {
      errors.email = "Invalid email format";
      isValid = false;
    }

    if (!description.trim()) {
      errors.description = "Description is required";
      isValid = false;
    }

    setErrors(errors);
    return isValid;
  };

  const onSubmitForm = async (e) => {
    e.preventDefault();
    if (validateForm()) {
      try {
        const body = { username, title, email, description };
        const response = await fetch("http://localhost:5000/feedback", {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify(body),
        });
        window.location = "/";
      } catch (err) {
        console.error(err.message);
      }
    }
  };

  return (
    <Fragment>
      <h1 className="text-center mt-5">User List</h1>
      <form className="mt-5" onSubmit={onSubmitForm}>
        <table className="table table-bordered">
          <tbody>
            <tr>
              <td>Title</td>
              <td>
                <input
                  type="text"
                  className={`form-control ${errors.title && 'is-invalid'}`}
                  value={title}
                  onChange={(e) => setTitle(e.target.value)}
                />
                {errors.title && <div className="invalid-feedback">{errors.title}</div>}
              </td>
            </tr>
            <tr>
              <td>Username</td>
              <td>
                <input
                  type="text"
                  className={`form-control ${errors.username && 'is-invalid'}`}
                  value={username}
                  onChange={(e) => setUsername(e.target.value)}
                />
                {errors.username && <div className="invalid-feedback">{errors.username}</div>}
              </td>
            </tr>
            <tr>
              <td>Email</td>
              <td>
                <input
                  type="text"
                  className={`form-control ${errors.email && 'is-invalid'}`}
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
                {errors.email && <div className="invalid-feedback">{errors.email}</div>}
              </td>
            </tr>
            <tr>
              <td>Description</td>
              <td>
                <textarea
                  className={`form-control ${errors.description && 'is-invalid'}`}
                  rows="3"
                  value={description}
                  onChange={(e) => setDescription(e.target.value)}
                ></textarea>
                {errors.description && <div className="invalid-feedback">{errors.description}</div>}
              </td>
            </tr>
          </tbody>
        </table>
        <button type="submit" className="btn btn-success">
          Add
        </button>
      </form>
    </Fragment>
  );
};

export default InputFeedback;

