import React, { useState } from "react";
import './Login.scss'
import { Link } from "react-router-dom";
import logo from './Assets/logo.png';

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };

  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    // Validation logic
    if (!email || !password) {
      setError("Please fill in all fields.");
    } else if (!validateEmail(email)) {
      setError("Please enter a valid email address.");
    } else {
      // Call API for login or perform other authentication logic here
      // Reset form state
      setEmail("");
      setPassword("");
      setError("");
    }
  };

  const validateEmail = (email) => {
    // Email validation logic here
    // Example: using a regex pattern
    const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailPattern.test(email);
  };

  return (
    <div className="login">
    

      <div className="form">
      <img src={logo} className="logo" alt="logo" />
        {error && <p>{error}</p>}
        <form onSubmit={handleSubmit}>
          <label htmlFor="email">Email:</label>
          <input
            type="emails"
            id="email"
            value={email}
            onChange={handleEmailChange}
            required
          />
          <br />
          <br />
          <label htmlFor="password">Password:</label>
          <input
            type="password"
            id="password"
            value={password}
            onChange={handlePasswordChange}
            required
          />
          <br></br><br></br>
          <Link to="/forgotpassword" style={{ textDecoration: "none" }}>
              <h4 type="submit">Forgot Password</h4>
          </Link>
          <br />
          <br />
          {/* <button type="submit">Login</button> */}
          <Link to="/home" >
              <button type="submit" >Login</button>
          </Link>
        </form>
      </div>
    </div>
  );
};

export default Login;








// import React, { useState } from "react";
// import "./Login.scss"; // import your customized CSS file
// import { Link } from "react-router-dom";

// function Login() {
//   const [username, setUsername] = useState("");
//   const [password, setPassword] = useState("");
//   const [forgotPassword, setForgotPassword] = useState(false);
//   const [email, setEmail] = useState("");

//   const handleLoginSubmit = (e) => {
//     e.preventDefault();
//     console.log("Username:", username, "Password:", password);
//     // submit your login form data here
//   };

//   const handleForgotPasswordSubmit = (e) => {
//     e.preventDefault();
//     console.log("Email:", email);
//     // submit your forgot password form data here
//   };

//   const handleForgotPasswordClick = () => {
//     setForgotPassword(true);
//   };

//   const handleBackClick = () => {
//     setForgotPassword(false);
//   };

//   return (
//     <div className="login-container">
//       <h1>Login</h1>
//       {forgotPassword ? (
//         <>
//           <p>Enter your email address to reset your password:</p>
//           <form onSubmit={handleForgotPasswordSubmit}>
//             <div className="form-group">
//               <label htmlFor="email">Email</label>
//               <input
//                 type="email"
//                 id="email"
//                 name="email"
//                 value={email}
//                 onChange={(e) => setEmail(e.target.value)}
//               />
//             </div>
//             <button type="submit">Reset Password</button>
//             <button type="button" onClick={handleBackClick}>Back</button>
//           </form>
//         </>
//       ) : (
//         <>
//           <form onSubmit={handleLoginSubmit}>
//             <div className="form-group">
//               <label htmlFor="username">Username</label>
//               <input
//                 type="text"
//                 id="username"
//                 name="username"
//                 value={username}
//                 onChange={(e) => setUsername(e.target.value)}
//               />
//             </div>
//             <div className="form-group">
//               <label htmlFor="password">Password</label>
//               <input
//                 type="password"
//                 id="password"
//                 name="password"
//                 value={password}
//                 onChange={(e) => setPassword(e.target.value)}
//               />
//             </div>
//             <Link to = "/home">
//             <button type="submit">Login</button>
//             </Link>
//           </form>
        
//           <button type="button" onClick={handleForgotPasswordClick}>Forgot Password?</button>
          
//         </>
//       )}
//     </div>
//   );
// }

// export default Login;
