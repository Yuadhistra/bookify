import React, { Fragment, useEffect, useState } from "react";
import styles from './FeedbackTable.module.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import styled from 'styled-components';

const DeleteButton = styled.button`
  background-color: #F67280;
  color: white;
  padding: 10px 20px;
  font-size: 14px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  height: 36px; 
  width: 120px; 

  &:hover {
    background-color: #D2042D;
  }

  @media (max-width: 768px) {
    font-size: 14px;
    padding: 0 16px;
  }

  @media (max-width: 769px) {
    font-size: 14px;
    padding: 0 16px;
  }

  @media (max-width: 500px) {
    font-size: 14px;
    width: 100%;
  }
`;

const Listuserfeedback = () => {
  const [feedback, setFeedback] = useState([]);


  const deleteFeedback = async id => {
    try {
      const deleteFeedback = await fetch(`http://localhost:5000/feedback/${id}`, {
        method: "DELETE"
      });

      setFeedback(feedback.filter(feedback => feedback.userfeedback_id !== id));
      toast.success('Feedback details deleted Successfully.')
      setTimeout(()=>{
        window.location = '/feedback';
      },2000);
    } catch (err) {
      console.error(err.message);
      toast.error('Feedback details can not delete.')
      setTimeout(()=>{
        window.location = '/feedback';
      },2000);
    }
  };

  const getFeedback = async () => {
    try {
      const response = await fetch("http://localhost:5000/feedback");
      const jsonData = await response.json();
      setFeedback(jsonData);
    } catch (err) {
      console.error(err.message);
    }
  };

  useEffect(() => {
    getFeedback();
  }, []);

  console.log(feedback);
  return (
    <Fragment>
    <ToastContainer position='top-center'/>
      <h1 className='text-center mt-5'>Feedback table</h1>
      <table className={`${styles.table} mt-5`}>
      <thead>
        <tr>
          <th className={styles.tableHeader}>Title</th>
          <th className={styles.tableHeader}>Username</th>
          <th className={styles.tableHeader}>Email</th>
          <th className={styles.tableHeader}>Description</th>
          <th className={styles.tableHeader}>Action</th>
        </tr>
      </thead>
      <tbody>
        {feedback.map((feedback) => (
          <tr key={feedback.userfeedback_id}>
            <td className={styles.tableData}>{feedback.title}</td>
            <td className={styles.tableData}>{feedback.username}</td>
            <td className={styles.tableData}>{feedback.email}</td>
            <td className={styles.tableData}>{feedback.description}</td>
            <td className={styles.tableData}>
              <DeleteButton onClick={() => deleteFeedback(feedback.userfeedback_id)} >
                Delete
              </DeleteButton>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
    </Fragment>
  );
};

export default Listuserfeedback;