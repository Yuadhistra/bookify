import React,{useState, Fragment} from "react";
import './inputbook.scss';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import styled from 'styled-components';

const Button = styled.button`
  background-color: #4d9b0e;
  color: white;
  padding: 10px 20px;
  font-size: 14px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  height: 36px; 
  width: 120px; 

  &:hover {
    background-color: #47e43e;
  }

  @media (max-width: 768px) {
    font-size: 14px;
    padding: 0 16px;
  }

  @media (max-width: 769px) {
    font-size: 14px;
    padding: 0 16px;
  }

  @media (max-width: 500px) {
    font-size: 14px;
    width: 100%;
  }
`;

const categoryOption=[
    {value:'arts',label:'Arts'},
    {value:'biography',label:'Biography'},
    {value:'romance',label:'Romance'},
    {value:'thriller',label:'Thriller'},
    {value:'crime',label:'Crime'},
    {value:'movies',label:'Movies'},
    {value:'religious',label:'Religious'},
    {value:'reference',label:'Reference'},
    {value:'novel',label:'Novel'},
]

const statusOption=[
  {value:'available',label:'Available'},
  {value:'not available',label:'Not Available'},
]

const InputBook =()=> {
    const[name,setName]=useState('')
    const[author,setAuthor]=useState('')
    const[price,setPrice]=useState('')
    const[type,setType]=useState('')
    const[rating,setRating]=useState('')
    const[status,setStatus]=useState('')
    const[description,setDescription]=useState('')
    const[image,setImage]=useState(null)

    const [nameError, setNameError] = useState("");
    const [authorError, setAuthorError] = useState("");
    const [priceError, setPriceError] = useState("");
    const [typeError, setTypeError] = useState("");
    const [ratingError, setRatingError] = useState("");
    const [statusError, setStatusError] = useState("");
    const [descriptionError, setDescriptionError] = useState("");
    const [imageError, setImageError] = useState("");

    const onSubmitForm = async (e) => {
    e.preventDefault();

    // Validate form fields before submission
    if (!validateForm()) {
      return;
    }

    try {
      const formData = new FormData();
      formData.append("name", name);
      formData.append("author", author);
      formData.append("price", price);
      formData.append("rating", rating);
      formData.append("status", status);
      formData.append("type", type);
      formData.append("description", description);
      formData.append("image", image);

      const response = await fetch("http://localhost:5000/books", {
        method: "POST",
        body: formData,
      });
      toast.success("Add book Successfully.");
      setTimeout(() => {
        window.location = "/product";
      }, 2000);
    } catch (err) {
      console.error(err.message);
    }
  };

  const validateForm = () => {
    let isValid = true;

    setNameError("");
    setAuthorError("");
    setPriceError("");
    setTypeError("");
    setRatingError("");
    setStatusError("");
    setDescriptionError("");
    setImageError("");

    if (!name) {
      setNameError("Name is required");
      isValid = false;
    }

    if (!author) {
      setAuthorError("Author is required");
      isValid = false;
    }

    if (!price) {
      setPriceError("Price is required");
      isValid = false;
    } else if (isNaN(price)) {
      setPriceError("Price must be a number");
      isValid = false;
    }

    if (!type) {
      setTypeError("Type is required");
      isValid = false;
    }

    if (!rating) {
      setRatingError("Rating is required");
      isValid = false;
    } else if (isNaN(rating) || rating < 0 || rating > 5) {
      setRatingError("Rating must be a number between 0 and 5");
      isValid = false;
    }

    if (!status) {
      setStatusError("Status is required");
      isValid = false;
    }

    if (!description) {
      setDescriptionError("Description is required");
      isValid = false;
    }

    if (!image) {
      setImageError("Image is required");
      isValid = false;
    } else if (!image.type.startsWith("image")) {
      setImageError("Invalid file type. Only image files are allowed.");
      isValid = false;
    }
    return isValid;
  };

    return(
      <Fragment>
      <ToastContainer position="top-center" />
      <h1 className="text-center mt-5">Add Book</h1>
      <form
        className="mt-5"
        onSubmit={onSubmitForm}
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <table className="table table-bordered">
          <tbody>
            <tr>
              <td>Name of Book</td>
              <td>
                <input
                  type="text"
                  className={`form-control ${nameError ? "is-invalid" : ""}`}
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                />
                {nameError && <div className="invalid-feedback">{nameError}</div>}
              </td>
            </tr>
            <tr>
              <td>Author</td>
              <td>
                <input
                  type="text"
                  className={`form-control ${authorError ? "is-invalid" : ""}`}
                  value={author}
                  onChange={(e) => setAuthor(e.target.value)}
                />
                {authorError && <div className="invalid-feedback">{authorError}</div>}
              </td>
            </tr>
            <tr>
              <td>Price</td>
              <td>
                <input
                  type="text"
                  className={`form-control ${priceError ? "is-invalid" : ""}`}
                  value={price}
                  onChange={(e) => setPrice(e.target.value)}
                />
                {priceError && <div className="invalid-feedback">{priceError}</div>}
              </td>
            </tr>
            <tr>
              <td>Rating</td>
              <td>
                <input
                  type="text"
                  className={`form-control ${ratingError ? "is-invalid" : ""}`}
                  value={rating}
                  onChange={(e) => setRating(e.target.value)}
                />
                {ratingError && <div className="invalid-feedback">{ratingError}</div>}
              </td>
            </tr>
            <tr>
              <td>Status</td>
              <td>
                <select
                  id="status"
                  className={`form-control ${statusError ? "is-invalid" : ""}`}
                  value={status}
                  onChange={(e) => setStatus(e.target.value)}
                >
                  <option value="">Select Status</option>
                  {statusOption.map((option) => (
                    <option key={option.value} value={option.value}>
                      {option.label}
                    </option>
                  ))}
                </select>
                {statusError && <div className="invalid-feedback">{statusError}</div>}
              </td>
            </tr>
            <tr>
              <td>Category</td>
              <td>
                <select
                  name="Select Category"
                  id="category"
                  className={`form-control ${typeError ? "is-invalid" : ""}`}
                  value={type}
                  onChange={(e) => setType(e.target.value)}
                >
                  <option value="">Select Category</option>
                  {categoryOption.map((option) => (
                    <option key={option.value} value={option.value}>
                      {option.label}
                    </option>
                  ))}
                </select>
                {typeError && <div className="invalid-feedback">{typeError}</div>}
              </td>
            </tr>
            <tr>
              <td>Description</td>
              <td>
                <textarea
                  className={`form-control ${descriptionError ? "is-invalid" : ""}`}
                  rows="3"
                  value={description}
                  onChange={(e) => setDescription(e.target.value)}
                ></textarea>
                {descriptionError && <div className="invalid-feedback">{descriptionError}</div>}
              </td>
            </tr>
            <tr>
              <td>Image</td>
              <td>
                <input
                  placeholder="Image"
                  onChange={(e) => setImage(e.target.files[0])}
                  type="file"
                  required=""
                  className={`form-control ${imageError ? "is-invalid" : ""}`}
                />
                {imageError && <div className="invalid-feedback">{imageError}</div>}
              </td>
            </tr>
            <tr>
              <td>Action</td>
              <td>
                <Button type="submit" className="btn btn-success">
                  Add
                </Button>
              </td>
            </tr>
          </tbody>
        </table>
      </form>
    </Fragment>
)
}

export default InputBook;