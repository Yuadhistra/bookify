import React, { Fragment, useState } from "react";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import styled from 'styled-components';

const EditButton = styled.button`
  background-color: #4d9b0e;
  color: white;
  padding: 10px 20px;
  font-size: 14px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  height: 36px; 
  width: 120px; 

  &:hover {
    background-color: #47e43e;
  }

  @media (max-width: 768px) {
    font-size: 14px;
    padding: 0 16px;
  }

  @media (max-width: 769px) {
    font-size: 14px;
    padding: 0 16px;
  }

  @media (max-width: 500px) {
    font-size: 14px;
    width: 100%;
  }
`;

const categoryOption = [
  { value: 'arts', label: 'Arts' },
  { value: 'biography', label: 'Biography' },
  { value: 'romance', label: 'Romance' },
  { value: 'thriller', label: 'Thriller' },
  { value: 'crime', label: 'Crime' },
  { value: 'movies', label: 'Movies' },
  { value: 'religious', label: 'Religious' },
  { value: 'reference', label: 'Reference' },
];

const statusOption = [
  { value: 'available', label: 'Available' },
  { value: 'not available', label: 'Not Available' },
];

const EditBook = ({ book }) => {
  const [name, setName] = useState(book.name);
  const [author, setAuthor] = useState(book.author);
  const [price, setPrice] = useState(book.price);
  const [type, setType] = useState(book.type);
  const [rating, setRating] = useState(book.rating);
  const [status, setStatus] = useState(book.status);
  const [description, setDescription] = useState(book.description);
  const [image, setImage] = useState(book.image);

  const updateBook = async (e) => {
    e.preventDefault();
    
    // Check if required fields are empty
    if (!name || !author || !price || !type || !rating || !status || !description) {
      toast.error('Please fill in all the required fields.');
      return;
    }
  
    try {
      const formData = new FormData();
      formData.append('name', name);
      formData.append('author', author);
      formData.append('price', price);
      formData.append('rating', rating);
      formData.append('status', status);
      formData.append('type', type);
      formData.append('description', description);
  
      if (image) {
        formData.append('image', image);
      } else {
        formData.append('image', book.image);
      }
      const response = await fetch(`http://localhost:5000/books/${book.book_id}`, {
        method: 'PUT',
        body: formData,
      });
  
      if (response.ok) {
        toast.success('Successfully updated.');
        setTimeout(() => {
          window.location = '/update';
        }, 2000);
      } else {
        console.error('Update failed:', response.statusText);
      }
    } catch (err) {
      console.error(err.message);
    }
  };
  

  return (
    <Fragment>
    <ToastContainer position='top-center'/>
      <EditButton
        data-toggle="modal"
        data-target={`#id${book.book_id}`}
      >
        Edit
      </EditButton>

      <div className="modal" id={`id${book.book_id}`}>
        <div className="modal-dialog modal-lg" style={{ height: '10rem' }}>
          <div className="modal-content">
            <div className="modal-header">
              <h4 className="modal-title">Edit book</h4>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                onClick={() => {
                  setName(book.name);
                  setAuthor(book.author);
                  setPrice(book.price);
                  setType(book.type);
                  setRating(book.rating);
                  setStatus(book.status);
                  setDescription(book.description);
                  setImage(book.image);
                }}
              >
                &times;
              </button>
            </div>
            <div className="modal-body">
              <form className="mt-5" onSubmit={updateBook}>
                <table className="table table-bordered">
                  <tbody>
                    <tr>
                      <td>Name of Book</td>
                      <td>
                        <input
                          type="text"
                          className="form-control"
                          value={name}
                          onChange={(e) => setName(e.target.value)}
                        />
                      </td>
                    </tr>

                    <tr>
                      <td>Author</td>
                      <td>
                        <input
                          type="text"
                          className="form-control"
                          value={author}
                          onChange={(e) => setAuthor(e.target.value)}
                        />
                      </td>
                    </tr>

                    <tr>
                      <td>Price</td>
                      <td>
                        <input
                          type="text"
                          className="form-control"
                          value={price}
                          onChange={(e) => setPrice(e.target.value)}
                        />
                      </td>
                    </tr>

                    <tr>
                      <td>Rating</td>
                      <td>
                        <input
                          type="text"
                          className="form-control"
                          value={rating}
                          onChange={(e) => setRating(e.target.value)}
                        />
                      </td>
                    </tr>

                    <tr>
                      <td>Status</td>
                      <td>
                        <select
                          id="status"
                          value={status}
                          onChange={(e) => setStatus(e.target.value)}
                        >
                          <option value="">Select Status</option>
                          {statusOption.map((option) => (
                            <option key={option.value} value={option.value}>
                              {option.label}
                            </option>
                          ))}
                        </select>
                      </td>
                    </tr>

                    <tr>
                      <td>Category</td>
                      <td>
                        <select
                          id="category"
                          value={type}
                          onChange={(e) => setType(e.target.value)}
                        >
                          <option value="">Select Category</option>
                          {categoryOption.map((option) => (
                            <option key={option.value} value={option.value}>
                              {option.label}
                            </option>
                          ))}
                        </select>
                      </td>
                    </tr>

                    <tr>
                      <td>Description</td>
                      <td>
                        <textarea
                          className="form-control"
                          rows="3"
                          value={description}
                          onChange={(e) => setDescription(e.target.value)}
                        ></textarea>
                      </td>
                    </tr>

                    <tr>
                      <td>Image</td>
                      <td>
                        <input
                          placeholder="Image"
                          onChange={(e) => setImage(e.target.files[0])}
                          type="file"
                          required=""
                        />
                      </td>
                    </tr>
                  </tbody>
                </table>
              </form>

              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-success"
                  data-dismiss="modal"
                  onClick={(e) => updateBook(e)}
                >
                  Update
                </button>
                <button
                  type="button"
                  className="btn btn-danger"
                  data-dismiss="modal"
                  onClick={() => {
                    setName(book.name);
                    setAuthor(book.author);
                    setPrice(book.price);
                    setType(book.type);
                    setRating(book.rating);
                    setStatus(book.status);
                    setDescription(book.description);
                    setImage(book.image);
                  }}
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default EditBook;
