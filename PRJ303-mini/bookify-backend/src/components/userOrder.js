import React, { Fragment, useEffect, useState } from "react";
import styles from './FeedbackTable.module.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import styled from 'styled-components';

const DeleteButton = styled.button`
  background-color: #F67280;
  color: white;
  padding: 10px 20px;
  font-size: 14px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  height: 36px; 
  width: 120px; 

  &:hover {
    background-color: #D2042D;
  }

  @media (max-width: 768px) {
    font-size: 14px;
    padding: 0 16px;
  }

  @media (max-width: 769px) {
    font-size: 14px;
    padding: 0 16px;
  }

  @media (max-width: 500px) {
    font-size: 14px;
    width: 100%;
  }
`;

const ListuserOrder = () => {
  const [order, setOrder] = useState([]);


  const deleteOrder= async id => {
    try {
      const deleteOrder = await fetch(`http://localhost:5000/order/${id}`, {
        method: "DELETE"
      });

      setOrder(order.filter(order => order.order_id !== id));
      toast.success('Order details deleted Successfully.')
      setTimeout(()=>{
        window.location = '/order';
      },2000);
    } catch (err) {
      console.error(err.message);
    }
  };

  const getOrder = async () => {
    try {
      const response = await fetch("http://localhost:5000/order");
      const jsonData = await response.json();
      setOrder(jsonData);
    } catch (err) {
      console.error(err.message);
    }
  };

  useEffect(() => {
    getOrder();
  }, []);

  console.log(order);
  return (
    <Fragment>
    <ToastContainer position='top-center'/>
      <h1 className='text-center mt-5'>User Order table</h1>
      <table className={`${styles.table} mt-5`}>
      <thead>
        <tr>
          <th className={styles.tableHeader}>Username</th>
          <th className={styles.tableHeader}>Email</th>
          <th className={styles.tableHeader}>address</th>
          <th className={styles.tableHeader}>Book Details</th>
          <th className={styles.tableHeader}>Total</th>
          <th className={styles.tableHeader}>Action</th>
        </tr>
      </thead>
      <tbody>
        {order.map((order) => (
          <tr key={order.order_id}>
            <td className={styles.tableData}>{order.username}</td>
            <td className={styles.tableData}>{order.email}</td>
            <td className={styles.tableData}>{order.address}</td>
            <td className={styles.tableData}>{order.bookdetails}</td>
            <td className={styles.tableData}>Nu.{order.total}</td>
            <td className={styles.tableData}>
              <DeleteButton onClick={() => deleteOrder(order.order_id)} >
                Delete
              </DeleteButton>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
    </Fragment>
  );
};

export default ListuserOrder;