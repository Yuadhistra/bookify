import React, { Fragment, useEffect, useState } from "react";
import EditBook from "./EditBook";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import styled from 'styled-components';

const DeleteButton = styled.button`
  background-color: #F67280;
  color: white;
  padding: 10px 20px;
  font-size: 14px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  height: 36px; 
  width: 120px; 

  &:hover {
    background-color: #D2042D;
  }

  @media (max-width: 768px) {
    font-size: 14px;
    padding: 0 16px;
  }

  @media (max-width: 769px) {
    font-size: 14px;
    padding: 0 16px;
  }

  @media (max-width: 500px) {
    font-size: 14px;
    width: 100%;
  }
`;

const ListBook = () => {
  const [books, setBooks] = useState([]);

  const deleteBook = async (id) => {
    try {
      const deleteBook = await fetch(`http://localhost:5000/books/${id}`, {
        method: "DELETE"
      });

      setBooks(books.filter(book => book.book_id !== id));
      toast.success('Book delete Successfully.')
      setTimeout(() => {
        window.location = '/update';
      }, 2000);
    } catch (err) {
      console.error(err.message);
    }
  };

  const getBooks = async () => {
    try {
      const response = await fetch("http://localhost:5000/books");
      const jsonData = await response.json();
      setBooks(jsonData);
    } catch (err) {
      console.error(err.message);
    }
  };

  useEffect(() => {
    getBooks();
  }, []);

  return (
    <Fragment>
      <ToastContainer position='top-center' />
      <h1 className='text-center mt-5'>Update Book Details</h1>
      <table className="table mt-5 text-center">
        <thead>
          <tr>
            <th>Name</th>
            <th>Author</th>
            <th>Price</th>
            <th>Category</th>
            <th>Rating</th>
            <th>Status</th>
            <th>Description</th>
            <th>Image</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {books.map(book => (
            <tr key={book.book_id}>
              <td>{book.name}</td>
              <td>{book.author}</td>
              <td>{book.price}</td>
              <td>{book.type}</td>
              <td>{book.rating}</td>
              <td>{book.status}</td>
              <td>{book.description}</td>
              <td>
                {book.image && (
                  <img
                    src={`http://localhost:5000/image/${book.image}`}
                    alt=""
                    style={{ maxWidth: '100px', maxHeight: '100px' }}
                  />
                )}
              </td>
              <td>
                <EditBook book={book} />
                <DeleteButton
                  className="btn btn-danger"
                  onClick={() => deleteBook(book.book_id)}
                >
                  Delete
                </DeleteButton>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </Fragment>
  );
};

export default ListBook;