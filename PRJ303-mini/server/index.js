const express = require("express");
const app = express();
const cors = require("cors");
const pool = require("./db");
const multer = require("multer");
const bcrypt = require('bcrypt');
// const session = require('express-session');
var session = require('express-session')

app.use(express.urlencoded({ extended: true }))

// var app = express()
app.set('trust proxy', 1) // trust first proxy
app.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true,
  cookie: { secure: true }
}))

// Middleware
app.use(cors());
app.use(express.json());

// Serve uploaded images
app.use("/image", express.static("./uploads"));

const storage = multer.diskStorage({
  destination: "./uploads", 
  filename: (req, file, cb) => {
    cb(null, `image-${file.originalname}`); 
  },
});

const fileFilter = (req, file, cb) => {
  if (file.mimetype.startsWith("image")) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

// Initialize multer upload object
const upload = multer({
  storage: storage,
  limits: { fileSize: 1024 * 1024 * 50 },
  fileFilter: fileFilter,
});

// Create a book
app.post("/books", upload.single("image"), async (req, res) => {
  try {
    if (req.file) {
      const { filename } = req.file;
      const { name, author, price, type, rating, status, description } = req.body;
      const newBook = await pool.query(
        "INSERT INTO book (name, author, price, type, rating, status, description, image) VALUES($1, $2, $3, $4, $5, $6, $7, $8) RETURNING *",
        [name, author, price, type, rating, status, description, filename]
      );
      res.json(newBook.rows[0]);
    } else {

      res.status(400).json({ message: "No file uploaded" });
    }
  } catch (err) {
    console.error(err.message);
    res.status(500).json({ message: "Server Error" });
  }
});

// Get a book by ID
app.get("/books/:id", async (req, res) => {
  try {
    const { id } = req.params;
    const book = await pool.query("SELECT * FROM book WHERE book_id = $1", [id]);
    if (book.rows.length === 0) {
      return res.status(404).json({ message: "Book not found" });
    }
    res.json(book.rows[0]);
  } catch (err) {
    console.error(err.message);
    res.status(500).json({ message: "Server Error" });
  }
});

// Get all books
app.get("/books", async (req, res) => {
  try {
    const allBooks = await pool.query("SELECT * FROM book");
    res.json(allBooks.rows);
  } catch (err) {
    console.error(err.message);
    res.status(500).json({ message: "Server Error" });
  }
});

// Update a book
app.put("/books/:id", upload.single("image"), async (req, res) => {
  try {
    const { id } = req.params;
    const { name, author, price, type, rating, status, description } = req.body;

    let filename = "";
    if (req.file) {
      filename = req.file.filename;
    } else {
      const book = await pool.query("SELECT image FROM book WHERE book_id = $1", [id]);
      filename = book.rows[0].image;
    }

    const updateBook = await pool.query(
      "UPDATE book SET name = $1, author = $2, price = $3, type = $4, rating = $5, status = $6, description = $7, image = $8 WHERE book_id = $9",
      [name, author, price, type, rating, status, description, filename, id]
    );

    res.json({ message: "Book was updated!" });
  } catch (err) {
    console.error(err.message);
    res.status(500).json({ message: "Server Error" });
  }
});

// Delete a book
app.delete("/books/:id", async (req, res) => {
  try {
    const { id } = req.params;
    const deleteBook = await pool.query("DELETE FROM book WHERE book_id = $1", [id]);
    res.json({ message: "Book was deleted!" });
  } catch (err) {
    console.error(err.message);
    res.status(500).json({ message: "Server Error" });
  }
});

// Create user feedback
app.post("/feedback", async (req, res) => {
  try {
    const { username, title, email, description } = req.body;
    const newFeedback = await pool.query(
      "INSERT INTO userfeedback (username, title, email, description) VALUES($1, $2, $3, $4) RETURNING *",
      [username, title, email, description]
    );
    res.json(newFeedback.rows[0]);
  } catch (err) {
    console.error(err.message);
    res.status(500).json({ message: "Server Error" });
  }
});

// Get all user feedback
app.get("/feedback", async (req, res) => {
  try {
    const allFeedback = await pool.query("SELECT * FROM userfeedback");
    res.json(allFeedback.rows);
  } catch (err) {
    console.error(err.message);
    res.status(500).json({ message: "Server Error" });
  }
});

// Delete user feedback
app.delete("/feedback/:id", async (req, res) => {
  try {
    const { id } = req.params;
    const deleteuserfeedback = await pool.query("DELETE FROM userfeedback WHERE userfeedback_id = $1", [id]);
    res.json({ message: "Feedback was deleted!" });
  } catch (err) {
    console.error(err.message);
    res.status(500).json({ message: "Server Error" });
  }
});

// Create user details
app.post("/userdetails", async (req, res) => {
  try {
    const { email, username, phonenumber, address, password } = req.body;
    const newUser = await pool.query(
      "INSERT INTO bookuser (email, username, phonenumber, address, password) VALUES($1, $2, $3, $4, $5) RETURNING *",
      [email, username, phonenumber, address, password]
    );
    res.json(newUser.rows[0]);
  } catch (err) {
    console.error(err.message);
    res.status(500).json({ message: "Server Error" });
  }
});

// Get all user details
app.get("/userdetails", async (req, res) => {
  try {
    const allUsers = await pool.query("SELECT * FROM bookuser");
    res.json(allUsers.rows);
  } catch (err) {
    console.error(err.message);
    res.status(500).json({ message: "Server Error" });
  }
});

// Get user details by email
app.get("/userdetails", async(req,res)=>{
    console.log(req)
    const email = req.params
    try {
        const allDetails = await pool.query("SELECT * FROM bookuser WHERE email = $1", [email]);
        res.json(allDetails.rows)
    } catch (err) {
        console.error(err.message)
    }
})
// Delete user details
app.delete("/userdetails/:id", async (req, res) => {
  try {
    const { id } = req.params;
    const deletebookuser = await pool.query("DELETE FROM bookuser WHERE email_id = $1", [id]);
    res.json({ message: "User details was deleted!" });
  } catch (err) {
    console.error(err.message);
    res.status(500).json({ message: "Server Error" });
  }
});

//Register
app.post('/signup', (req, res) => {
  const { email, firstName, password, phonenumber } = req.body;

  bcrypt.hash(password, 10, (err, hash) => {
    if (err) {
      console.error(err);
      res.status(500).json({ message: 'Internal server error' });
    } else {
      pool.query(
        'INSERT INTO bookuser (email, First_name, hash_password, phonenumber) VALUES ($1, $2, $3, $4)',
        [email, firstName,hash, phonenumber],
        (err, result) => {
          if (err) {
            console.error(err);
            res.status(500).json({ message: 'Internal server error' });
          } else {
            console.log('User added');
            res.status(200).json({ message: 'User added' });
          }
        }
      );
    }
  });
});

//api to get users
app.get('/signup',async(req,res)=>{
  try {
      const user = await pool.query('SELECT * FROM bookuser')
      res.json(user.rows)
  } catch (error) {
      console.error(error)
      
  }
})

// Login
app.post('/login', async (req, res) => {
  const { email, password } = req.body;

  // Check if user with given email exists in the database
  const result = await pool.query('SELECT * FROM bookuser WHERE email = $1', [email]);
  const user = result.rows[0];

  if (!user) {
    return res.status(401).json({ message: 'Invalid email or password' });
  }

  // Check if the provided password matches the hashed password stored in the database
  const passwordMatches = await bcrypt.compare(password, user.hash_password);

  if (!passwordMatches) {
    return res.status(401).json({ message: 'Invalid email or password' });
  }

  console.log("login");

  // Set the user session
  req.session.user = {
    email: user.email,
    username: user.first_name
  };

  const username = req.session.user.username;
  console.log(username);

  res.status(200).json({ email: user.email, username });
});


//Admin Register
app.post('/adminsignup', (req, res) => {
  const { email, firstName, password, phonenumber } = req.body;

  bcrypt.hash(password, 10, (err, hash) => {
    if (err) {
      console.error(err);
      res.status(500).json({ message: 'Internal server error' });
    } else {
      pool.query(
        'INSERT INTO adminuser (email, First_name, hash_password, phonenumber) VALUES ($1, $2, $3, $4)',
        [email, firstName,hash, phonenumber],
        (err, result) => {
          if (err) {
            console.error(err);
            res.status(500).json({ message: 'Internal server error' });
          } else {
            console.log('User added');
            res.status(200).json({ message: 'User added' });
          }
        }
      );
    }
  });
});

//api to get users
app.get('/adminsignup',async(req,res)=>{
  try {
      const user = await pool.query('SELECT * FROM adminuser')
      res.json(user.rows)
  } catch (error) {
      console.error(error)
      
  }
})

app.post('/adminlogin', async (req, res) => {
  const { email, password } = req.body;

  // Check if user with given email exists in the database
  const result = await pool.query('SELECT * FROM adminuser WHERE email = $1', [email]);
  const user = result.rows[0];

  if (!user) {
    return res.status(401).json({ message: 'Invalid email or password' });
  }

  // Check if the provided password matches the hashed password stored in the database
  const passwordMatches = await bcrypt.compare(password, user.hash_password);

  if (!passwordMatches) {
    return res.status(401).json({ message: 'Invalid email or password' });
  }

  console.log("login");

  // Set the user session
  req.session.user = {
    email: user.email,
    username: user.first_name
  };

  const username = req.session.user.username;
  console.log(username);

  res.status(200).json({ email: user.email, username });
});


/////////////////////////////////////////////////////////////
//My order
app.post("/order", async (req, res) => {
  try {
    const { username,email,address, bookdetails, total } = req.body;
    const newOrder = await pool.query(
      "INSERT INTO myorder (username,email,address, bookdetails, total) VALUES($1, $2, $3, $4, $5) RETURNING *",
      [username,email,address, bookdetails, total]
    );
    res.json(newOrder.rows[0]);
  } catch (err) {
    console.error(err.message);
    res.status(500).json({ message: "Server Error" });
  }
});

// Get all user feedback
app.get("/order", async (req, res) => {
  try {
    const allOrder = await pool.query("SELECT * FROM myorder");
    res.json(allOrder.rows);
  } catch (err) {
    console.error(err.message);
    res.status(500).json({ message: "Server Error" });
  }
});

// Delete user feedback
app.delete("/order/:id", async (req, res) => {
  try {
    const { id } = req.params;
    const deleteOrder = await pool.query("DELETE FROM myorder WHERE order_id = $1", [id]);
    res.json({ message: "Order details was deleted!" });
  } catch (err) {
    console.error(err.message);
    res.status(500).json({ message: "Server Error" });
  }
});


app.listen(5000, () => {
  console.log("Server has started on port 5000");
});
