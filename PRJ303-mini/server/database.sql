CREATE DATABASE btable;

CREATE TABLE book(
    book_id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    author VARCHAR(255),
    price SMALLINT,
    type VARCHAR(255) NOT NULL,
    rating SMALLINT,
    status VARCHAR(255),
    description VARCHAR(2550)
);

CREATE TABLE bookuser(
    email_id SERIAL PRIMARY KEY,
   email VARCHAR(255) UNIQUE,
   username VARCHAR(255),
	phonenumber VARCHAR(255),
   address VARCHAR(255),
	password VARCHAR(255) NOT NULL,
	confirm VARCHAR(255) NOT NULL
);

CREATE TABLE userFeedback(
    userfeedback_id SERIAL PRIMARY KEY,
    title VARCHAR(255),
    email VARCHAR(255),
    description VARCHAR(2550)
);

CREATE TABLE myorder(
   order_id SERIAL PRIMARY KEY,
    username VARCHAR(255),
    email VARCHAR(255),
    address VARCHAR(255),
	bookdetails VARCHAR(2550),
	total VARCHAR(255)
);

CREATE TABLE buy(
    order_id SERIAL PRIMARY KEY,
	phonenumber VARCHAR(255),
   address VARCHAR(255),
	list VARCHAR(2555),
   total VARCHAR(255)
);

CREATE TABLE adminuser(
    admin_id SERIAL PRIMARY KEY,
	email VARCHAR(255),
   password VARCHAR(255)
);